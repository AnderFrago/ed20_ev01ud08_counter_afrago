package org.cuatrovientos.ed.counter.util;

public class Counter {
	
	/***
	 * Recibimos un valor 5,
	 * tenemos que sumar 1+2+3+4+5 = 15
	 * @param num
	 * @return
	 */
	public int count(int num) {
		int resultado=0;
		// Operación compleja
		for (int i = 0; i < num; i++) {
			resultado +=i;
		}
		return resultado;
	}

}
