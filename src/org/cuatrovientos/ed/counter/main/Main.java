package org.cuatrovientos.ed.counter.main;

import java.util.Scanner;

import org.cuatrovientos.ed.counter.util.Counter;

public class Main {

	
	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		System.out.println("Introduce un n�mero");	
		int valor = lector.nextInt();
		
		Counter c = new Counter();
		int suma = c.count(valor);
		
		System.out.println("El resultado es "+suma);
	}

}
